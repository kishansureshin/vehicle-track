package com.example.vehtrackparentapp;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ShowMessageActivity extends Activity {
	
	TextView tv1,loc_tv,time_tv;
	Button show_loc;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_message);
		tv1 = (TextView) findViewById(R.id.textView1);
		loc_tv = (TextView) findViewById(R.id.tv_loc);
		time_tv = (TextView) findViewById(R.id.tv_time);
		show_loc = (Button)findViewById(R.id.show_loc_btn);
		
		Bundle bundle = getIntent().getExtras();
		
		String message = bundle.getString("message");
		List<String> details_split = Arrays.asList(message.split("!"));
		String speed = details_split.get(0);
//		String lat_lng = details_split.get(2);
		String time = details_split.get(1);
		
		tv1.setText(speed.substring(3));
		time_tv.setText(time);
		
		Toast.makeText(ShowMessageActivity.this, "Enable Data and click the button to show location", Toast.LENGTH_LONG).show();
		
		show_loc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Bundle bundle = getIntent().getExtras();
				String message = bundle.getString("message");
				List<String> details_split = Arrays.asList(message.split("!"));
				String lat_lng = details_split.get(2);
				Toast.makeText(ShowMessageActivity.this, lat_lng, Toast.LENGTH_SHORT).show();
				if (lat_lng != "null:null") {
					List<String> loc = Arrays.asList(lat_lng.split(":"));
					/*------- To get city name from coordinates -------- */
			          String city = null;
			          String state = null;
			          String country = null;
			          String postalCode = null;
			          Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
			          List<Address> addresses;
			          try {
			        	  
			        		  addresses = gcd.getFromLocation(Double.parseDouble(loc.get(0)),
				                      Double.parseDouble(loc.get(1)), 1);
				              if (addresses.size() > 0) {
				                  System.out.println(addresses.get(0).getLocality());
				                  city = addresses.get(0).getLocality();
				                  state = addresses.get(0).getAdminArea();
				                  country = addresses.get(0).getCountryName();
				                  postalCode = addresses.get(0).getPostalCode();
//				                  cityName = addresses.get(0).getLocality();
				              }
			              
			          }
			          catch (IOException e) {
			              e.printStackTrace();
			          }
			          String s ="\n\nMy Current City is: "
			              + city + "\n\n State : " + state + "\n\n Country : " + country + "\n\n Postal Code : " + postalCode;
//			          editLocation.setText(s);
			          loc_tv.setText(s);
				} else {
					Toast.makeText(ShowMessageActivity.this, "Location is null and not available", Toast.LENGTH_SHORT).show();
				}
				
			}
		});
		
		
		 
		
	}

}
