package com.example.vehtrackparentapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
	
	TextView tv;
	EditText child_name,child_phone;
	Button save,list_users,logout;
	public static final String MyPREFERENCES = "MyPrefs" ;
//	public static final String user = "userKey";
//	public static final String Phone = "phoneKey";
	SharedPreferences sharedpreferences;
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        child_name = (EditText) findViewById(R.id.child_name);
        child_phone = (EditText) findViewById(R.id.child_phone);
        save = (Button) findViewById(R.id.save);
        list_users = (Button) findViewById(R.id.list_btn);
        logout = (Button) findViewById(R.id.logout_btn);

        logout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
        
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				String child_fname = child_name.getText().toString();
				String child_phone_no = child_phone.getText().toString();
				SharedPreferences.Editor editor = sharedpreferences.edit();
				int size = sharedpreferences.getAll().size();
//				Toast.makeText(MainActivity.this, String.valueOf(size), Toast.LENGTH_SHORT).show();

				size++;
				editor.putString(String.valueOf(size), child_fname+":"+child_phone_no);

				editor.commit();
				Toast.makeText(MainActivity.this,"Saved Successfully",Toast.LENGTH_LONG).show();
				child_name.setText("");
				child_phone.setText("");
			}
		});
        
        list_users.setOnClickListener(new OnClickListener() {
        	
			@Override
			public void onClick(View v) {
	
				Intent i = new Intent(MainActivity.this,ListChildrenActivity.class);
				startActivity(i);
				
			}
		});
    }
  
}
