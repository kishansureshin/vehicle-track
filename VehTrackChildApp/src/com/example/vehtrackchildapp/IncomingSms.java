package com.example.vehtrackchildapp;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.telephony.gsm.SmsMessage;
import android.util.Log;
import android.widget.Toast;
//import android.text.format.Formatter;



public class IncomingSms extends BroadcastReceiver  {
	
	
	public static final String MyPREFERENCES = "MyPrefs" ;
	// Get the object of SmsManager
    final SmsManager sms = SmsManager.getDefault();
    int count_received = 0;
	@Override
	public void onReceive(Context context, Intent intent) {
		// Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();
 
        try {
             
            if (bundle != null) {
                 
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                 
                for (int i = 0; i < pdusObj.length; i++) {
                     
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    SharedPreferences prefs = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    String senderNum = phoneNumber.substring(3);

                    String message = currentMessage.getDisplayMessageBody();
                    
                    int phoneNoFoundFlag = 0;
                    Map<String,?> keys = prefs.getAll();
            		for(Map.Entry<String,?> entry : keys.entrySet()){
                        String childDetail = (String) entry.getValue();
                        if (!entry.getKey().equals("user_password")) {
                        	List<String> child_details_split = Arrays.asList(childDetail.split(":"));
        					String savedPhoneNo = child_details_split.get(1);
        					if (savedPhoneNo.equals(senderNum)) {
    							phoneNoFoundFlag = 1;
    						}
						}
            		}
                    
            		
               	 
                    if (message.equals("speed_location")) {
                    	count_received = 1; 
                    	Intent in = new Intent();
                     	
                         in.setClassName("com.example.vehtrackchildapp", "com.example.vehtrackchildapp.CalculateSpeed");
                         in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                         in.putExtra("message", message+":"+phoneNumber);
                         in.putExtra("count_received", count_received);
                         context.startActivity(in);
                    	 
					}
                     
                } // end for loop
              } // bundle is null
 
        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);
             
        }
	}

	
	
	
	
	
}
