package com.example.vehtrackchildapp;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
//import java.util.logging.Formatter;

public class CalculateSpeed extends Activity implements IBaseGpsListener {
	public static final String MyPREFERENCES = "MyPrefs" ;
	TextView tv_location,tv_loc_area_code;
	int count = 0,count_received = 0,count_sms=36000;
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calculate_speed);
		
		tv_location = (TextView) findViewById(R.id.tv_location);
		
		
		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        this.updateSpeed(null,null,null,null);
       
        CheckBox chkUseMetricUntis = (CheckBox) this.findViewById(R.id.chkMetricUnits);
        chkUseMetricUntis.setOnCheckedChangeListener(new OnCheckedChangeListener() {
             
              @Override
              public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // TODO Auto-generated method stub
                    CalculateSpeed.this.updateSpeed(null,null,null,null);
              }
        });
       
	
	}
	
	public void finish()
    {
          super.finish();
          System.exit(0);
    }

    private void updateSpeed(CLocation location,Double lat,Double lng,String cityName) {
          // TODO Auto-generated method stub
          float nCurrentSpeed = 0;
         
          if(location != null)
          {
                location.setUseMetricunits(this.useMetricUnits());
                nCurrentSpeed = location.getSpeed();
          }
         
          Formatter fmt = new Formatter(new StringBuilder());
          fmt.format(Locale.UK, "%5.1f", nCurrentSpeed);
          String strCurrentSpeed = fmt.toString();
          strCurrentSpeed = strCurrentSpeed.replace(' ', '0');
          
          float speed_in_km = Float.parseFloat(strCurrentSpeed);
          speed_in_km = (float) (speed_in_km * 1.609344);
         
          String strUnits = "km/hour";
          if(this.useMetricUnits())
          {
                strUnits = "meters/second";
          }
         
          TextView txtCurrentSpeed = (TextView) this.findViewById(R.id.txtCurrentSpeed);
          txtCurrentSpeed.setText(speed_in_km + " " + strUnits);
          
          String time = DateFormat.getDateTimeInstance().format(new Date());
          
          String msg = "+*@ Speed : " + speed_in_km + "\nCity Name : "+ cityName + "! Time : "+ time +"!"+lat+":"+lng;
         
        	  try {
            	  Bundle bundle = getIntent().getExtras();
          		  String message = bundle.getString("message");
          		  
          		  
          		if ((message != null && lat != null) && (count_sms % 36000 == 0)) 
          		{
//          			int count_sms = bundle.getInt("count_received");
          			Toast.makeText(CalculateSpeed.this, msg+"count : "+count, Toast.LENGTH_SHORT).show();
          			List<String> msgArr = Arrays.asList(message.split(":"));
          			String phoneNo = msgArr.get(1);
          			SmsManager smsManager = SmsManager.getDefault();
          			smsManager.sendTextMessage(phoneNo, null, msg, null, null);
          			message = null;
          			Intent intent = new Intent(Intent.ACTION_MAIN);
    				intent.addCategory(Intent.CATEGORY_HOME);
    				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    				startActivity(intent);
    				count_sms++;
        		} 
          		
    			
	    		} catch (Exception e) {
//	    			Toast.makeText(CalculateSpeed.this, e.getMessage(), Toast.LENGTH_SHORT).show();
	    		}

          
          
        if((count % 3600 == 0) || speed_in_km > 60)
  		{
    			SharedPreferences prefs = getApplicationContext().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//  			Toast.makeText(CalculateSpeed.this, msg, Toast.LENGTH_SHORT).show();
  			Map<String,?> keys = prefs.getAll();
      		for(Map.Entry<String,?> entry : keys.entrySet()){
                  String childDetail = (String) entry.getValue();
                  if (!entry.getKey().equals("user_password")) {
                  	List<String> child_details_split = Arrays.asList(childDetail.split(":"));
  					String savedPhoneNo = child_details_split.get(1);
  					SmsManager smsManager = SmsManager.getDefault();
  		  			smsManager.sendTextMessage(savedPhoneNo, null, msg, null, null);
  				}
      		}
  		}
        count++;
          
    }

    private boolean useMetricUnits() {
          // TODO Auto-generated method stub
          CheckBox chkUseMetricUnits = (CheckBox) this.findViewById(R.id.chkMetricUnits);
          return chkUseMetricUnits.isChecked();
    }

    @Override
    public void onLocationChanged(Location location) {
          // TODO Auto-generated method stub
//          if(location != null)
//          {
//                CLocation myLocation = new CLocation(location, this.useMetricUnits());
//                this.updateSpeed(myLocation,location.getLatitude(),location.getLongitude());
//          }
//          Toast.makeText(
//                  getBaseContext(),
//                  "Location changed: Lat: " + location.getLatitude() + " Lng: "
//                      + location.getLongitude(), Toast.LENGTH_SHORT).show();
          String longitude = "Longitude: " + location.getLongitude();
//          Log.v(TAG, longitude);
          String latitude = "Latitude: " + location.getLatitude();
//          Log.v(TAG, latitude);

          /*------- To get city name from coordinates -------- */
          String cityName = null;
          Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
          List<Address> addresses;
          try {
              addresses = gcd.getFromLocation(location.getLatitude(),
                      location.getLongitude(), 1);
              if (addresses.size() > 0) {
                  System.out.println(addresses.get(0).getLocality());
                  cityName = addresses.get(0).getLocality();
              }
          }
          catch (IOException e) {
              e.printStackTrace();
          }
          String s = longitude + "\n" + latitude + "\n\nMy Current City is: "
              + cityName;
//          editLocation.setText(s);
          tv_location.setText(s);
          if(location != null)
          {
                CLocation myLocation = new CLocation(location, this.useMetricUnits());
                this.updateSpeed(myLocation,location.getLatitude(),location.getLongitude(),cityName);
          }
         
  		
  		
    }

    @Override
    public void onProviderDisabled(String provider) {
          // TODO Auto-generated method stub
         
    }

    @Override
    public void onProviderEnabled(String provider) {
          // TODO Auto-generated method stub
         
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
          // TODO Auto-generated method stub
         
    }

    @Override
    public void onGpsStatusChanged(int event) {
          // TODO Auto-generated method stub
         
    }
}
