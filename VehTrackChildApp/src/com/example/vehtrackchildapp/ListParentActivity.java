package com.example.vehtrackchildapp;

import java.util.ArrayList;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListParentActivity extends Activity {
	
	static int count = 0;
	ListView childrenList;
	private ArrayAdapter<String> listAdapter ; 
	public static final String MyPREFERENCES = "MyPrefs" ;
	public static final String user = "userKey";
//	SharedPreferences sharedpreferences;
	
	Editor editor;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_parent);
		
		SharedPreferences prefs = ListParentActivity.this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		
		editor = prefs.edit();
		
		childrenList = (ListView) findViewById(R.id.childList);
				
		ArrayList<String> childrenListArr = new ArrayList<String>();
		childrenListArr.clear();


		Map<String,?> keys = prefs.getAll();
		for(Map.Entry<String,?> entry : keys.entrySet()){
//            Log.d("map values",entry.getKey() + ": " + 
//                                   entry.getValue().toString());
            String childDetail = (String) entry.getValue();
            childrenListArr.add(entry.getKey()+"."+childDetail);
		}

	    listAdapter = new ArrayAdapter<String>(this,
	            R.layout.simplerow, childrenListArr);
	    childrenList.setAdapter(listAdapter);
	    
	    childrenList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String child_details = (String) childrenList.getItemAtPosition(position);
				Toast.makeText(ListParentActivity.this, child_details, Toast.LENGTH_SHORT).show();
				
				Intent i = new Intent(ListParentActivity.this,SingleParentSettingActivity.class);
				
				i.putExtra("child_details",child_details);
				startActivity(i);
			}	    	
		});
		
	}
	

	
	@Override
	protected void onRestart() {
		super.onRestart();
		setContentView(R.layout.activity_list_parent);
		
		SharedPreferences prefs = ListParentActivity.this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		
		editor = prefs.edit();
		
		childrenList = (ListView) findViewById(R.id.childList);
				
		ArrayList<String> childrenListArr = new ArrayList<String>();
		childrenListArr.clear();


		Map<String,?> keys = prefs.getAll();
		for(Map.Entry<String,?> entry : keys.entrySet()){
            Log.d("map values",entry.getKey() + ": " + 
                                   entry.getValue().toString());
            String childDetail = (String) entry.getValue();
            childrenListArr.add(entry.getKey()+"."+childDetail);
		}

	    listAdapter = new ArrayAdapter<String>(this,
	            R.layout.simplerow, childrenListArr);
	    childrenList.setAdapter(listAdapter);
	    
	    childrenList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String child_details = (String) childrenList.getItemAtPosition(position);
				Toast.makeText(ListParentActivity.this, child_details, Toast.LENGTH_SHORT).show();
				Intent i = new Intent(ListParentActivity.this,SingleParentSettingActivity.class);
				i.putExtra("child_details",child_details);
				startActivity(i);
			}	    	
		});
	}
	
}
