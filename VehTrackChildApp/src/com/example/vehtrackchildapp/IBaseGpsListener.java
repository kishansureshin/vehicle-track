package com.example.vehtrackchildapp;

import android.location.GpsStatus.Listener;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

public interface IBaseGpsListener extends LocationListener, Listener {

	public void onLocationChanged(Location location);
    
    public void onProviderDisabled(String provider);
   
    public void onProviderEnabled(String provider);
   
    public void onStatusChanged(String provider, int status, Bundle extras);
   
    public void onGpsStatusChanged(int event);
	
}
