package com.example.vehtrackchildapp;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.gsm.SmsManager;

public class ShutdownReceiver extends BroadcastReceiver {

	
	public static final String MyPREFERENCES = "MyPrefs" ;
	@Override
	public void onReceive(Context arg0, Intent arg1) {
		
		SharedPreferences prefs = arg0.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		Map<String,?> keys = prefs.getAll();
  		for(Map.Entry<String,?> entry : keys.entrySet()){
              String childDetail = (String) entry.getValue();
              if (!entry.getKey().equals("user_password")) {
              	List<String> child_details_split = Arrays.asList(childDetail.split(":"));
					String savedPhoneNo = child_details_split.get(1);
					SmsManager smsManager = SmsManager.getDefault();
		  			smsManager.sendTextMessage(savedPhoneNo, null, "Your child tries to switch off the device", null, null);
				}
  		}
	}

}
