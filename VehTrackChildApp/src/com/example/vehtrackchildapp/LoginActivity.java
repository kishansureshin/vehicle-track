package com.example.vehtrackchildapp;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
	
	Button login_btn,cancel_btn;
	EditText username_ed,passord_ed;
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;
	Editor editor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_login);
		
		login_btn = (Button)findViewById(R.id.button);

		passord_ed = (EditText)findViewById(R.id.editText2);
		cancel_btn = (Button)findViewById(R.id.button2);
		
		cancel_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		login_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String password_entered = passord_ed.getText().toString();
				SharedPreferences prefs = LoginActivity.this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
				editor = prefs.edit();
				String storedPassword = prefs.getString("user_password", null);
				if (storedPassword == null) {
					Toast.makeText(LoginActivity.this, "You are the first user and the password is saved successfully", Toast.LENGTH_LONG).show();
					editor.putString("user_password", password_entered);
					editor.commit();
					
				} else if (password_entered.equals(storedPassword)) {
					Intent i = new Intent(LoginActivity.this,MainActivity.class);
					startActivity(i);
				} 
				else {
					Toast.makeText(LoginActivity.this, "Not Authorized user", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

}
