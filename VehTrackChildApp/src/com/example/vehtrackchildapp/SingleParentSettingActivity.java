package com.example.vehtrackchildapp;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class SingleParentSettingActivity extends Activity  {
	
	Button req_btn,del_btn;
//	public static final String user = "userKey";
	public static final String MyPREFERENCES = "MyPrefs" ;
	Editor editor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parent_setting);
		
		SharedPreferences prefs = SingleParentSettingActivity.this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		editor = prefs.edit();
		

		del_btn = (Button) findViewById(R.id.btn_delete);
		
		
		del_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Bundle bundle = getIntent().getExtras();
				String child_details = bundle.getString("child_details");
				
				String str_pos = child_details.substring(0,1);
				editor.remove(str_pos);
				editor.commit();
				Toast.makeText(SingleParentSettingActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
			}
		});
		
		
	}
	

}
